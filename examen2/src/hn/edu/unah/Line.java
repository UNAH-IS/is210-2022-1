package hn.edu.unah;

import java.util.Objects;

public class Line {
    // Atributos
    private double a;
    private double b;
    private double c;

    public Line(double x, double y, double c) {
        this.a = x;
        this.b = y;
        this.c = c;
    }

    @Override
    public String toString() {
        return String.format("%.2fx + %.2fy = %.2f", a, b, c);
    }

    public boolean esVertical() {
        return b == 0.0;
    }

    public boolean esHorizontal() {
        return a == 0.0;
    }

    public double pendiente() {
        if (!esVertical()) {
            return -a/b;
        } else {
            return Double.MAX_VALUE;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Line line = (Line) o;
        boolean prueba1 = Double.compare(line.a, a) == 0
                && Double.compare(line.b, b) == 0
                && Double.compare(line.c, c) == 0;

        if (prueba1) {
            return true;
        }

        double k = this.a / line.a;

        return Double.compare(k * line.a, a) == 0
                && Double.compare(k * line.b, b) == 0
                && Double.compare(k * line.c, c) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, c);
    }

    public static boolean sonParalelas(Line line1, Line line2) {
        // Son verticales las dos
        if (line1.esVertical() && line2.esVertical()) {
            return true;
        }

        return (line1.pendiente() == line2.pendiente());
    }

    public static boolean sonPerpendiculares(Line line1, Line line2) {
        if (line1.esHorizontal() && line2.esVertical()
                || line1.esVertical() && line2.esHorizontal()) {
            return true;
        }

        return (line1.pendiente() * line2.pendiente() == -1.0);
    }

    public static void interseccion(Line line1, Line line2) {
        if (Line.sonParalelas(line1, line2)) {
            System.out.println("Son paralelas");
            return;
        }
        /*
        Paso 1: dividir entre b tanto a como c => a/bx + y = c/b
        Paso 2: cambiar el signo de a => y = -a/bx + c/b
        Esto queda como y = pendiente * x + c/b
         */
        Line line1Norm = new Line(line1.pendiente(), 1, line1.c / line1.b);
        Line line2Norm = new Line(line2.pendiente(), 1, line2.c / line2.b);

        /*
        Teniendo las dos lineas normalizadas solo es ejecutar la siguiente
        igualdad:

        ax + c = dx + e
        ax - dx = e - c
        x = (e - c) / (a - d)
         */
        double x = (line2Norm.c - line1Norm.c) / (line1Norm.a - line2Norm.a);

        /*
        Ahora que tenemos x, es posible obtener y al intercambiar los valores,
        esto puede ser en las lineas normalizadas para mayor facilidad pero se
        puede realizar en las lineas originales.

        y = ax + c
        */
        double y = line1Norm.a * x + line1Norm.c;

        System.out.printf("El punto de interseccion es (%.2f, %.2f)%n", x, y);
    }
}
