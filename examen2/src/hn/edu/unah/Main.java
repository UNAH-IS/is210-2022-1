package hn.edu.unah;

public class Main {

    public static void main(String[] args) {
	    Line l1 = new Line(2, 3, 0);
        System.out.println(l1);

        Line l2 = new Line(1, 1.5, 0);

        System.out.println(l1.equals(l2));

        Line lVertical = new Line(2, 0, 1);
        Line lHorizontal = new Line(0, 1, -1);

        System.out.println("Son perpendiculares: "
                + Line.sonPerpendiculares(lVertical, lHorizontal));

        System.out.println("Son paralelas l1 y l2? "
                + Line.sonParalelas(l1, l2));

        System.out.println("Prueba de interseccion");

        Line la = new Line(2, 4, 1);
        Line lb = new Line(3, 1, -1);

        Line.interseccion(la, lb);
    }
}
