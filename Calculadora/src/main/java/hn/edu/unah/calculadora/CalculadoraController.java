package hn.edu.unah.calculadora;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class CalculadoraController implements Initializable {
    @FXML
    private Label resultado;

    @FXML
    private TextField valor1;

    @FXML
    private TextField valor2;

    @FXML
    private ComboBox operador;

    public void calcularButtonClick() {
        double val1 = Double.parseDouble(valor1.getText());
        double val2 = Double.parseDouble(valor2.getText());
        double res = val1 + val2;
        // TODO: Utilizar el combobox para gestionar las decisiones
        System.out.println(operador.getValue());
        resultado.setText("" + res);

    }

    ObservableList<String> lista = FXCollections.observableArrayList(
            "sumar",
            "restar",
            "multiplicar",
            "dividir"
    );

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        operador.setItems(lista);
    }
}