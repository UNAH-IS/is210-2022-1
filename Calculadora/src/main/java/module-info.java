module hn.edu.unah.calculadora {
    requires javafx.controls;
    requires javafx.fxml;


    opens hn.edu.unah.calculadora to javafx.fxml;
    exports hn.edu.unah.calculadora;
}