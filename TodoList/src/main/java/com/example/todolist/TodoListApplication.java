package com.example.todolist;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class TodoListApplication extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent pane = FXMLLoader.load(
                getClass().getResource("login-view.fxml")
        );
        Scene scene = new Scene(pane, 300, 300);
        primaryStage.setTitle("Todo List");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}


// FXMLoader -> load -> scene
// FXMLoader -> load -> Parent -> scene