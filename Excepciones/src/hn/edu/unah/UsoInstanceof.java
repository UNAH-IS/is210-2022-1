package hn.edu.unah;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UsoInstanceof {
    public static void main(String[] args) {
        boolean debeTerminar = true;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Escriba dos numeros para dividirlos");

        do {
            try {
                int num1 = scanner.nextInt();
                int num2 = scanner.nextInt();
                System.out.println(num1 / num2);
                debeTerminar = true;


                int[] arr = new int[3];

                for (int i = 0; i < 5; i++) {
                    arr[i] = i;
                }
            }
            catch (Exception ex) {
                if (ex instanceof ArithmeticException) {
                    System.out.println("Division entre cero");
                }
                else if (ex instanceof InputMismatchException) {
                    System.out.println("Establezca un numero por favor");
                    debeTerminar = false;
                    // Limpiar los tokens
                    scanner.nextLine();
                }
                else if (ex instanceof ArrayIndexOutOfBoundsException) {
                    System.out.println(ex.getMessage());
                }
                else {
                    System.out.println("Capturado por Exception: " + ex.getMessage());
                }
            }
        } while (!debeTerminar);

        System.out.println("FIN DEL PROGRAMA");
    }
}
