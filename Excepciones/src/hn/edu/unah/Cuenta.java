package hn.edu.unah;

public class Cuenta {
    private String codigo;
    private double saldo;
    private final int RETIRO_MAXIMO = 5000;

    public Cuenta(String codigo, double saldo) {
        this.codigo = codigo;
        this.saldo = saldo;
    }

    public void depositar(double monto) {
        this.saldo += monto;
    }

    public void retirar(double monto) throws CuentaSobregiroException, CuentaMontoMaximoException {
        if (monto > RETIRO_MAXIMO) {
            throw new CuentaMontoMaximoException(RETIRO_MAXIMO);
        }
        if (monto > this.saldo) {
            throw new CuentaSobregiroException();
        }

        this.saldo -= monto;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "saldo=" + saldo +
                ", codigo='" + codigo + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Cuenta c = new Cuenta("012", 3000);

        try {
            c.retirar(3001);
        } catch (CuentaSobregiroException e) {
            e.printStackTrace();
        } catch (CuentaMontoMaximoException e) {
            e.printStackTrace();
        }
        System.out.println(c);

    }
}

// Clases derivadas de Exception para gestionar reglas de negocio (requerimientos)
class CuentaSobregiroException extends Exception {
    public CuentaSobregiroException() {
        super("Retiro de cuenta invalido: monto superior al saldo");
    }
}

class CuentaMontoMaximoException extends Exception {
    public CuentaMontoMaximoException(double monto) {
        super(String.format("El monto maximo para retiro debe ser: L%.2f", monto));
    }
}