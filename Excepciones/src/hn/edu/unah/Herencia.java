package hn.edu.unah;

import java.time.LocalDate;

public class Herencia {
    public static void main(String[] args) {
        Trabajador trabajador = new Trabajador(
                "Mauricio Lopez", "ENEE", "electricista");
        trabajador.nacionalidad = "hondureño";
        System.out.println(trabajador);
        trabajador.trabajar();

        Persona persona = trabajador;
        System.out.println(persona);
        // persona.trabajar();
    }
}


class Persona {
    public String nombre;
    private String identidad;
    protected LocalDate fechaNacimiento;
    String nacionalidad;

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", nacionalidad='" + nacionalidad + '\'' +
                '}';
    }
}

class Trabajador extends Persona {
    private String empresa;
    private String cargo;
    private double salario;

    public Trabajador(String nombre, String empresa, String cargo) {
        // inicializar la parte correspondiente a la persona dentro del trabajador
        super(nombre);
        this.empresa = empresa;
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Trabajador{" +
                "nombre='" + nombre + '\'' +
                ", empresa='" + empresa + '\'' +
                ", cargo='" + cargo + '\'' +
                '}';
    }

    public void trabajar() {
        System.out.println("Estoy trabajando de 9:00 a 17:00");
    }
}