package hn.edu.unah;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ParteI {

    public static void main(String[] args) {
        boolean debeTerminar = true;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Escriba dos numeros para dividirlos");

        do {
            try {
                int num1 = scanner.nextInt();
                int num2 = scanner.nextInt();
                System.out.println(num1 / num2);
                debeTerminar = true;


                int[] arr = new int[3];

                for (int i = 0; i < 5; i++) {
                    arr[i] = i;
                }
            } catch (ArithmeticException arithmeticException) {
                System.out.println("Division entre cero");
            } catch (InputMismatchException inputMismatchException) {
                System.out.println("Establezca un numero por favor");
                debeTerminar = false;
                // Limpiar los tokens
                scanner.nextLine();
            } catch (ArrayIndexOutOfBoundsException outOfBoundsException) {
                System.out.println(outOfBoundsException.getMessage());
            } catch (Exception ex) {
                System.out.println("Capturado por Exception: " + ex.getMessage());
            }
        } while (!debeTerminar);

        System.out.println("FIN DEL PROGRAMA");
    }
}
