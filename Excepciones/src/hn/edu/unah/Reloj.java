package hn.edu.unah;

class HoraInvalidaException extends Exception {
    public HoraInvalidaException() {
        super("La hora es valida debe ser desde 0 hasta 11");
    }
}

public class Reloj {
    private int horas;
    private int minutos;
    private int segundos;
    private String meridiano;

    public Reloj(int horas, int minutos, int segundos, String meridiano)
            throws HoraInvalidaException {
        this.setHoras(horas);
        this.setMinutos(minutos);
        this.setSegundos(segundos);
        this.setMeridiano(meridiano);
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) throws HoraInvalidaException {
        if (horas < 0 || horas > 11) {
            throw new HoraInvalidaException();
        }
        this.horas = horas;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public String getMeridiano() {
        return meridiano;
    }

    public void setMeridiano(String meridiano) {
        this.meridiano = meridiano;
    }

    public static void main(String[] args) {
        int h = 13;
        int m = 45;
        int s = 56;
        String meridiano = "pm";

        try {
            Reloj reloj = new Reloj(h, m, s, meridiano);
        } catch (HoraInvalidaException e) {
            e.printStackTrace();
        }
    }
}
