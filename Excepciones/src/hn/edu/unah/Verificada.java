package hn.edu.unah;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Verificada {
    public static void abrirArchivoThrows() throws FileNotFoundException {
        // throws se coloca en la firma de una funcion
        Scanner scanner = new Scanner(new FileReader("xyz.txt"));
    }

    public static void abrirArchivoCatch() {
        try {
            Scanner scanner = new Scanner(new FileReader("xyz.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException controlado");
        }
    }

    public static void abrirArchivoThrow() {
        // throws se coloca en la firma de una funcion
        String ruta = "xyz.txt";
        try {
            Scanner scanner = new Scanner(new FileReader(ruta));
        } catch (FileNotFoundException e) {
            // Hago un procedimiento especifico para el FileNotFound
            // throw tambien sirve para lanzar excepciones pero desde el cuerpo de la funcion
            throw new RuntimeException("Archivo no encontrado: " + ruta);
        }
    }

    public static void main(String[] args) {
        abrirArchivoThrow();

        System.out.println("FIN DEL PROGRAMA");
    }
}
