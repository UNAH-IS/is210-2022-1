/*

Ruta absoluta en Windows
"C:\\Users\\Documents\\a.txt"

Rutas absoluta en sistemas basados en UNIX
"/home/Documents/a.txt"

FileNotFoundException - el archivo puede no encontrarse...
1. Atrapar la excepcion (colocarlo en un bucle try-catch)
2. Lanzar la excepcion a otro lugar que pueda manejar la excepcion (caso 1) o
que tambien la lance (caso 2). ******* (por los momentos)


 */

package hn.edu.unah.part1.fichero;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class Print {
    public static void main(String[] args) throws FileNotFoundException {
        String ruta = "files/conteo.dat";
        PrintStream printStream = new PrintStream(ruta);
        for (int i = 0; i < 50; i++) {
            printStream.printf("[Linea %d]%n", i);
        }

        printStream.close();
    }
}
