/*

Clases envoltorios o wrappers: Son clases que permiten crear objetos de aquellos
tipos de datos primitivos
- Boolean - boolean
- Byte - byte
- Short - short
- Character - char
- Integer - int
- Long - long
- Float - float
- Double - double


 */

package hn.edu.unah.part1.fichero;

import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Scanner;

public class EstadioLectura {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new FileReader("files/estadio.dat"));

        int boletosVendidos = 0;
        double ventaTotal = 0;

        while(scanner.hasNext()) {
            double precioBoletos = scanner.nextDouble();
            int cantidadBoletos = scanner.nextInt();

            boletosVendidos += cantidadBoletos;
            ventaTotal += precioBoletos * cantidadBoletos;
        }

        DecimalFormat floatFormat = new DecimalFormat("#,###.00");
        DecimalFormat intFormat = new DecimalFormat("#,###");
        String ventalTotalFormateada = floatFormat.format(ventaTotal);
        String boletosVendidosFormateado = intFormat.format(boletosVendidos);

        System.out.printf("Se vendieron %s boletos. El monto total fue de $ %s%n",
                boletosVendidosFormateado,
                ventalTotalFormateada);


        scanner.close();
    }
}
