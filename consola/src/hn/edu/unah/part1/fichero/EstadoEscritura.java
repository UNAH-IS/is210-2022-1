package hn.edu.unah.part1.fichero;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class EstadoEscritura {
    public static void main(String[] args) throws FileNotFoundException {
        /* Programa que solicite el tipo de asiento y el precio de cada asiento
         desde la consola y nos pida llenar cuanta gente ingreso con ese tipo de
         asiento.
        */

        // Declarar
        String[] tiposAsiento = new String[4];
        tiposAsiento[0] = "Palco";
        tiposAsiento[1] = "Silla";
        tiposAsiento[2] = "Sombra";
        tiposAsiento[3] = "Sol";

        Scanner consola = new Scanner(System.in);
        PrintStream ps = new PrintStream("files/estadio.dat");

        for (String tipo : tiposAsiento) {
            System.out.println("Coloque el costo del asiento tipo " + tipo);
            double costoAsiento = consola.nextDouble();
            System.out.println("Coloque cuantos compraron el asiento tipo " + tipo);
            int cantidad = consola.nextInt();

            ps.print(costoAsiento);
            ps.print(" ");
            ps.print(cantidad);
            ps.println();
        }

        ps.close();
        consola.close();


    }
}
