package hn.edu.unah.part1.fichero;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class ScannerDemo {
    public static void main(String[] args) throws FileNotFoundException {
        String ruta = "files/data.txt";
        Scanner entrada = new Scanner(new FileReader(ruta));

        System.out.println("LECTURA DE ARCHIVOS...");

        int i = 0;
        while(entrada.hasNext()) {
            System.out.println("Linea " + (++i));
            System.out.println(entrada.next());
        }

        entrada.close();
    }
}
