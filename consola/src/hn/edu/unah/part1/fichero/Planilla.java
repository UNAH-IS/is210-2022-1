package hn.edu.unah.part1.fichero;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class Planilla {
    public static void main(String[] args) throws IOException {

        String ruta = "C:\\java-projects\\IS210-2022-1-1800\\files\\datoEmpleado.txt";
        Scanner entrada = new Scanner(new FileReader(ruta));

        while(entrada.hasNext()) {
            String nombre = entrada.next();
            String apellido = entrada.next();
            double horas = entrada.nextDouble();
            double tarifa = Double.parseDouble(entrada.next());

            double salario = Math.round(horas * tarifa * 100.0) / 100.0;
            System.out.printf("Nombre: %s Apellido: %s Salario: %.2f (%f) %n", nombre, apellido,
                    salario, (horas * tarifa));

        }
        entrada.close();
    }
}


/*

    567.545369856774
    56754.53
    56754
    567.54

 */