package hn.edu.unah.part1.fichero;

import java.text.DecimalFormat;

public class Formateado {
    public static void main(String[] args) {
        DecimalFormat floatFormat = new DecimalFormat("#,###.00");

        System.out.println(
                floatFormat.format(3.4556446)
        );

        // double -> String
        String valorFormateado = floatFormat.format(3.4556446);
        System.out.println(valorFormateado);

        // String -> double
        double valor = Double.parseDouble(valorFormateado);
        System.out.println(valor + 1);
    }
}
