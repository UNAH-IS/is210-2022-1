package hn.edu.unah.part1.fichero;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintStream;
import java.util.Scanner;

public class Ch3Ex7 {
    public static void main(String[] args) throws FileNotFoundException {
        String rutaLectura = "files/Ch3_Ex7Data.txt";
        String rutaEscritura = "files/Ch3_Ex7Output.txt";
        Scanner scanner = new Scanner(new FileReader(rutaLectura));
        PrintStream printStream = new PrintStream(rutaEscritura);

        while (scanner.hasNext()) {
            String linea = scanner.nextLine();
            String[] tokens = linea.split(",");

            if (tokens.length < 4) {
                continue;
            }

            String apellido = tokens[0];
            String nombre = tokens[1];
            double salarioActual = Double.parseDouble(tokens[2]);
            double porcentajeAumento = Double.parseDouble(tokens[3]);
            double salarioAumentado = salarioActual * (1 + porcentajeAumento / 100);

            printStream.printf("%s %s %.2f%n", nombre, apellido, salarioAumentado);

            /*String apellido = scanner.next();
            String nombre = scanner.next();
            double salarioActual = scanner.nextDouble();
            double porcentajeAumento = scanner.nextDouble();*/
        }

        scanner.close();
        printStream.close();
    }
}
