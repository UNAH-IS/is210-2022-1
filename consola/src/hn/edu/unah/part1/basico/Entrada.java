package hn.edu.unah.part1.basico;

import java.util.Scanner;

public class Entrada {
    public static void main(String[] args) {
        // implementacion de un tipo primitivo
        int valor = 10;

        // implementacion de un tipo objeto
        String mensaje = "Hola";
        // new [function constructora]
        // System.in es un objeto de tipo InputStream <- teclado
        Scanner entrada = new Scanner(System.in).useDelimiter(",");
        System.out.print("Escriba una palabra: ");
        String dato = entrada.next();
        System.out.println("Primer token es: " + dato);

        // Limpiar de otros tokens que no han sido leidos
        String extra = entrada.nextLine();
        System.out.println("Los valores que se se perderias... " + extra);

        System.out.print("Escriba otra palabra: ");
        String segundoDato = entrada.next();
        System.out.println("Segundo token es: " + segundoDato);
    }
}
