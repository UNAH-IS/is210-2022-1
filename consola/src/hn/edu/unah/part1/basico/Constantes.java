package hn.edu.unah.part1.basico;

public class Constantes {
    public static void main(String[] args) {
        // variables
        int cuenta = 0;
        cuenta = 10;
        cuenta++;
        System.out.println(cuenta);

        // constantes: valores que una vez inicializados no se pueden modificar
        final int VALOR_MAXIMO = 100;
        //VALOR_MAXIMO = 200; no puede modificarse
    }
}
