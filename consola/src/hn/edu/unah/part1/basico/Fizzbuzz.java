/*
    Fizzbuzz
    ========
    - Listar los numeros del 1 al 100
    - Si un numero es divisible por 3 en lugar del numero colocar FIZZ
    - Si un numero es divisible por 5 colocar BUZZ
    - Si un numero es divisible por 3 y por 5 colocar FIZZBUZZ
    - Caso contrario colocar el numero correspondiente

1
2
FIZZ
4
BUZZ
FIZZ
7
8
FIZZ
BUZZ
11
FIZZ
13
14
FIZZBUZZ
16
17
FIZZ
 */

package hn.edu.unah.part1.basico;

public class Fizzbuzz {
    public static void main(String[] args) {
        for(int i = 1; i <= 100; i++) {
            if (i % 15 == 0) {
                System.out.println("FIZZBUZZ");
            } else if (i % 3 == 0) {
                System.out.println("FIZZ");
            } else if (i % 5 == 0) {
                System.out.println("BUZZ");
            } else {
                System.out.println(i);
            }
        }
    }
}
