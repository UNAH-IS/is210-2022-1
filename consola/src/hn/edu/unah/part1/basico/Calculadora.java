/*

Secuencias de control -> Tomar decisiones
    -if
        * if / else
        * if / else if / else
    -switch
    -[condicion-booleana] ? [verdadera] : [falsa] **
 */

package hn.edu.unah.part1.basico;

import java.util.Scanner;

public class Calculadora {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        System.out.println("Calculadora v1");
        System.out.println("Escriba un numero: ");
        int numero1 = entrada.nextInt();
        System.out.println("Escriba otro numero: ");
        int numero2 = entrada.nextInt();
        System.out.println("Escriba un operador...");
        System.out.println("+");
        System.out.println("-");
        System.out.println("*");
        System.out.println("/");
        System.out.println("%");
        String operador = entrada.next();

        // Calculo
        int resultado = 0;
        boolean esValidaElOperador = true;

        switch (operador) {
            case "+":
                resultado = numero1 + numero2;
                break;
            case "-":
                resultado = numero1 - numero2;
                break;
            case "*":
                resultado = numero1 * numero2;
                break;
            case "/":
                resultado = numero1 / numero2;
                break;
            case "%":
                resultado = numero1 % numero2;
                break;
            default:
                System.err.println("No se selecciono un operador valido");
                esValidaElOperador = false;
        }
        // Fin del calculo

        if (esValidaElOperador) {
            System.out.printf("El resultado de %d %s %d = %d", numero1, operador,
                    numero2, resultado);
        }

        entrada.close();
    }
}
