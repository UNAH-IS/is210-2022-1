/*
Las condiciones son boolean:
    if (condicion1) {
        // se cumple la condicion1
    } else {
        // NO se cumple la condicion1
    }

    --------------------------

    if (condicion1) {
        // se cumple la condicion1
    } else if (condicion2) {
        // se cumple la condicion2 (la condicion1 NO se cumplio)
    } else if (condicion3) {
        // se cumple la condicion3 (la condicion1 y la condicion2 NO se cumplen)
    } else {
        // todas las condiciones previas no se cumplieron
    }

    ---------------------------

    if (condicion1) {
        // la condicion1 se cumple
    }

    if (condicion2) {
        // la condicion2 se cumple
    }

    // Es posible que ambas condiciones se cumplan.

    --------------------------

    if (condicion1) {
        if (condicion2) {
            // se cumple la condicion2 (previo debe cumplirse la condicion1)
        }
    }

 */
package hn.edu.unah.part1.basico;

import java.util.Scanner;

public class Control {
    static void ejercicio1() {
    /*
    Escriba un programa que invite al usuario a ingresar un número. Luego el
    programa debe dar salida al número y a un mensaje diciendo si el número es
    positivo, negativo o cero.

    Entrada: numero [Scanner nextInt]
    Proceso: identificar si el numero es positivo/negativo/cero [if]
    Salida: numero / mensaje [print/printLn]
    */

        // [Scanner -> clase]
        // [entrada -> objeto]
        Scanner entrada = new Scanner(System.in);
        System.out.println("Ingrese un numero entero: ");
        int numero = entrada.nextInt();

        //  <xxxxxxxxxx 0 xxxxxxxxxxxx>
        if (numero > 0) {
            System.out.printf("El numero %d es positivo", numero);
        } else if (numero < 0) {
            System.out.printf("El numero %d es negativo", numero);
        } else {
            System.out.println("El numero es cero");
        }
    }

    static void ejercicio8() {
        Scanner entrada = new Scanner(System.in);
        System.out.println("Vamos a resolver la ecuacion ax^2 + bx + c");
        System.out.println("Indique el valor de a: ");
        int a = entrada.nextInt();
        System.out.println("Indique el valor de b: ");
        int b = entrada.nextInt();
        System.out.println("Indique el valor de c: ");
        int c = entrada.nextInt();

        if (a == 0) {
            System.out.println("El valor de a no es valido");
            return;
        }

        // discriminante = b^2 - 4ac
        double discriminante = Math.pow(b, 2) - (4 * a * c);

        if (discriminante == 0) {
            System.out.println("Tiene una sola raiz (repetida)");
            System.out.printf("La raiz es: %d", (-1 * b));
        } else if (discriminante > 0) {
            System.out.println("Tiene dos raices reales");
            double raiz1 = ((-1 * b) - Math.sqrt(discriminante)) / (2 * a);
            double raiz2 = ((-1 * b) + Math.sqrt(discriminante)) / (2 * a);
            System.out.printf("Raiz 1: %f y Raiz 2: %f", raiz1, raiz2);
        } else {
            System.out.println("Tiene dos raices complejas");
        }
    }

    public static void main(String[] args) {
        ejercicio8();

    }
}
