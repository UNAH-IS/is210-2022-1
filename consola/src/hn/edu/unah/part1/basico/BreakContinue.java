/*
    break -> saca bloque principal y lo coloca funcion contenedora
    continue ->  deja de evaluar (salta) las instrucciones debajok
 */

package hn.edu.unah.part1.basico;

import java.util.Scanner;

public class BreakContinue {
    public static void main(String[] args) {
        System.out.println("Diferencias entre continue, break y return");
        System.out.println("Se ejecutara un bucle del 1 al 100");
        System.out.println("Pero cuando vaya por el valor 50 si presiona");
        System.out.println("\t1. Ejecuta un continue");
        System.out.println("\t2. Ejecuta un break");
        System.out.println("\t3. Ejecuta un return");

        Scanner scanner = new Scanner(System.in);

        final int N = 100;
        int i = 0;
        int eleccion = scanner.nextInt();

        while (i <= N) {
            // Lo inicializo al inicio para que los no existan efectos secundarios
            // en los saltos
            i++;
            System.out.println(i);

            if (i >= 50) {
                if (eleccion == 1) {
                    System.out.println("Ejecutando un continue");
                    continue;
                } else if (eleccion == 2) {
                    System.out.println("Ejecutando un break");
                    break;
                } else if (eleccion == 3) {
                    System.out.println("Ejecutando un return");
                    return;
                }
            }
        }
        System.out.println("Fin del programa");
    }
}
