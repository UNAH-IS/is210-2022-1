/*

Repeticion
    - while
    - do-while
    - for

 */

package hn.edu.unah.part1.basico;

public class Repeticion {
    public static void main(String[] args) {
        int veces = 5;

        int i = 0;
        while(i < veces) {
            System.out.println("while");
            i++;
        }

        int j = 0;
        do {
            System.out.println("do-while");
            ++j;
        } while(j < veces);

        for (int k = 0; k < veces; k++) {
            System.out.println("for");
        }
    }
}
