/*
    La concatenacion masiva puede ser de las operaciones mas costosas en Java,
    ya que cada objeto crea uno nuevo

    Cadena                  tamaño      objetos creados
    ""                      0           1
    "" + "hola"             4           2
    "hola" + "hola"         8           3
    "holahola" + "hola"     12          4

    Por lo general concatenar Strings no es un problema. Esto puede ser poco
    eficiente si son muchos los objetos generados.
 */

package hn.edu.unah.part1.string;

public class Concatenacion {
    public static void main(String[] args) {
        String original = "hola";
        String concatenador = "";
        int N = 200_000;
        StringBuilder builder = new StringBuilder("");

        for (int i = 0; i < N; i++) {
            //concatenador += original;
            builder.append(original);
        }

        //System.out.println(concatenador);
    }
}
