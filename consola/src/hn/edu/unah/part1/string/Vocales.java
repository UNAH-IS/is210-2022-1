/*

Crear programa que contabilice cuantas vocales hay un String.

Etapas
---------
1. Descomponer el String en caracteres
2. Comparar si ese caracteres es una vocal
3. Si es una vocal actualizar una variable: conteo
4. Imprimir la varible conteo.

 */

package hn.edu.unah.part1.string;

import java.util.Scanner;

public class Vocales {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Escriba una frase: ");
        String frase = teclado.nextLine().toLowerCase();

        int vocales = 0;
        for (int i = 0; i < frase.length(); i++) {
            char caracter = frase.charAt(i);

            switch (caracter) {
                case 'a': case 'e': case 'i': case 'o': case 'u':
                    // En caso de no convertir en minusculas podria utilizar
                    // esta opcion
                    //case 'A': case 'E': case 'I': case 'O': case 'U':
                    vocales++;
            }
        }

        System.out.printf("La frase tiene %d vocales%n", vocales);

    }
}
