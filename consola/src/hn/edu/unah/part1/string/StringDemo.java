/*
    String es inmutable
        - no podemos modificar sus elementos internos
        - cualquier posible cambio lograr crear un nuevo objeto

    lowecase -> minusculas
    uppercase -> mayusculas

    s0 = "   adios         "

    s = s.trim().toUpperCase();

    s1 = "adios"
    s2 = "ADIOS"

 */

package hn.edu.unah.part1.string;

public class StringDemo {
    public static void main(String[] args) {
        String s = "hola";
        s = "   adios         ";
        char c = 'a';

        // Con trim() permite quitar espacios en blanco al inicio y al final
        // Con toUpperCase() convierte la cadena en mayusculas
        s = s.trim().toUpperCase();
        System.out.println("length() -> " + s.length());
        System.out.println("Contenido -> " + s);

        String t = " esto parece ser una despedida";
        String u = s + t;
        System.out.println(u);
    }
}
