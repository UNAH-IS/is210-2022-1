/*
    IN: "hola"
    OUT: "aloh"

 */

package hn.edu.unah.part1.string;

import java.util.Scanner;

public class Reverso {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Escriba una frase o palabra");
        String frase = teclado.nextLine();
        StringBuilder builder1 = new StringBuilder(frase);
        String reverso = String.valueOf(builder1.reverse());
        System.out.print("Este es el reverso: " + reverso);


    }
}
