/*
    Crear un programa que dada un String indique la cantidad de palabras
    que posee.

    IN: String
    OUT: Cantidad de palabras

    hola = 0 eb - 1 pal
    hola mundo = 1 eb - 2 pal
    como estan ustedes = 2 eb - 3 pal

 */
package hn.edu.unah.part1.string;

import java.util.Scanner;

public class Palabra {
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Escriba una frase: ");
        String frase = teclado.nextLine().trim();

        int espacioBlanco = 0;
        boolean espacioBlancoCalculado = false;

        for (int i = 0; i < frase.length(); i++) {
            char caracter = frase.charAt(i);
            if (caracter == ' ') {

                if (!espacioBlancoCalculado) {
                    espacioBlanco++;
                }

                espacioBlancoCalculado = true;
            } else {
                espacioBlancoCalculado = false;
            }
        }

        int palabras = espacioBlanco + 1;
        System.out.printf("Hay %d palabras%n", palabras);
    }
}
