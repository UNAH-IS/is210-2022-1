/*

    Formas de separar un String en sus elementos
    - char: charAt
    - String: substring
    - char[]: convertir el String en un array toCharArray

    HOLA MUNDO

    | H | O | L | A |   | M | U | N | D | O |   Valores (char)
    -----------------------------------------
    | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |   Indices (int)

 */

package hn.edu.unah.part1.string;

public class Elementos {
    public static void main(String[] args) {
        String s = "HOLA MUNDO";
        int indice = 9;
        char c = s.charAt(indice);
        int ascii = c;
        System.out.printf("charAt(%d) = %c%n", indice, c);
        System.out.println("El valor ascii es: " + ascii);

        // Para recorrerlos todos
        System.out.println("charAt");
        for (int i = 0; i < s.length(); i++) {
            char caracter = s.charAt(i);
            System.out.print(caracter + ", ");
        }

        System.out.println("\ntoCharArray - for");
        char[] caracteres = s.toCharArray();

        for (int i = 0; i < caracteres.length; i++) {
            char caracter = caracteres[i];
            System.out.print(caracter + ", ");
        }

        System.out.println("\ntoCharArray - foreach");
        for (char caracter : caracteres) {
            System.out.print(caracter + ", ");
        }

        System.out.println("\nsubstring");
        for (int i = 0; i < caracteres.length; i++) {
            String caracter = s.substring(i, i + 1);
            System.out.print(caracter + ", ");
        }
    }
}
