package hn.edu.unah.part1.object;


import java.awt.*;

public class Asignacion {
    public static void main(String[] args) {
        // autoboxing
        Object objeto = 10;
        objeto = "hola";

        // unboxing
        String saludo = (String) objeto;
        System.out.println(saludo);

        // copia de la direccion de memoria de un objeto a otro
        Point punto1 = new Point(10, 10);
        // Esto es una especie de unboxing
        Point punto2 = (Point) punto1.clone();
        punto1.x = 20;
        punto2.y = 20;
        System.out.println("\nObjeto1: " + punto1);
        System.out.println("\nObjeto2: " + punto2);
    }
}
