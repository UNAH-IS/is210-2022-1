package hn.edu.unah.part2.fraccion;

public class Fraccion {
    private int numerador;
    private int denominador;

    public Fraccion(int numerador, int denominador) {
        this.setNumerador(numerador);
        this.setDenominador(denominador);
    }

    public int getNumerador() {
        return numerador;
    }

    public void setNumerador(int numerador) {
        this.numerador = numerador;
    }

    public int getDenominador() {
        return denominador;
    }

    public void setDenominador(int denominador) {
        if (denominador != 0) {
            this.denominador = denominador;
        } else {
            this.denominador = 1;
        }

    }

    // Operaciones
    public static Fraccion sumar(Fraccion f1, Fraccion f2) {
        // a/b + c/d = (ad + bc) / bd
        int numerador = (f1.getNumerador() * f2.getDenominador() +
                        f1.getDenominador() * f2.getNumerador());
        int denominador = f1.getDenominador() * f2.getDenominador();
        Fraccion resultado = new Fraccion(numerador, denominador);
        return resultado;
    }

    // a < b

    @Override
    public String toString() {
        return String.format("%d/%d", numerador, denominador);
    }

    public static void main(String[] args) {
        Fraccion f1 = new Fraccion(1, 3);
        Fraccion f2 = new Fraccion(1, 2);
        Fraccion s = Fraccion.sumar(f1, f2);
        System.out.println(s);
    }
}
