package hn.edu.unah.part2.estaticos;

public class MainStatic {
    public static void main(String[] args) {
        // sumar no necesita un objeto debido a que es static
        int s = Aritmetica.sumar(10, 10);
        System.out.println(s);

        // En el caso de restar si necesita un objeto porque es no-static
        Aritmetica aritmetica = new Aritmetica();
        aritmetica.restar(10, 10);

        // Un caso comun es el paquete Math el cual contiene funciones static
        Math.cos(10);
    }
}
