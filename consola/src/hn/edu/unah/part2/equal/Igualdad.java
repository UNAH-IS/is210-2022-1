package hn.edu.unah.part2.equal;

import java.awt.*;

public class Igualdad {
    public static void main(String[] args) {
        Point punto1 = new Point(10, 10);
        Point punto2 = new Point(10, 10);

        // Copia superficial
        Point punto3 = punto1;

        // Compara si dos objetos son el mismo objeto
        System.out.println("Operador ==");
        System.out.println(punto1 == punto2);
        System.out.println(punto1 == punto3);

        System.out.println("Metodo equals");
        System.out.println(punto1.equals(punto2));
        System.out.println(punto1.equals(punto3));
    }
}
