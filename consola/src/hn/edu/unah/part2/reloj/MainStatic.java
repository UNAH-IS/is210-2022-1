package hn.edu.unah.part2.reloj;

public class MainStatic {
    public static void main(String[] args) {
        Reloj[] relojes = new Reloj[10];
        for (int i = 0; i < relojes.length; i++) {
            relojes[i] = new Reloj();
        }

        Reloj r1 = new Reloj();
        Reloj r2 = new Reloj();

        System.out.println(Reloj.secuencia);
    }
}

/*

propio objeto ----> package ---------> fuera del package
public              public              public
private
protected           protected
default             default


- public: visible en cualquier nivel
- protected: (sin contar la herencia) visible hasta el package en comun
- private: solo visible para el objeto/clase
- default: (no tiene modificador). Se le denomina de package

 */