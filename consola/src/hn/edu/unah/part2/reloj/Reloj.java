package hn.edu.unah.part2.reloj;

import java.util.Objects;

public class Reloj implements Cloneable {
    /**************************************************************************
                               Atributos / Datos
    **************************************************************************/

    // Atributo de clase (static)
    static int secuencia = 0;

    // Atributos de objetos
    final int MAX_HORAS = 23;
    final int MIN_HORAS = 0;
    final int MAX_MINUTOS = 59;
    final int MIN_MINUTOS = 0;
    final int MAX_SEGUNDOS = 59;
    final int MIN_SEGUNDOS = 0;

    private int segundos;
    private int minutos;
    private int horas;
    private EFormato formato;

    /**************************************************************************
                                Metodos / Funciones
     **************************************************************************/

    // Constructor
    public Reloj(int horas, int minutos, int segundos) {
        this.setHoras(horas);
        setMinutos(minutos);
        setSegundos(segundos);

        formato = EFormato.LARGO;

        // Llamado a un atributo de clase
        Reloj.secuencia++;
    }

    /*
    Caracteristicas de un constructor
    - public
    - no hay retorno (ni siquiera void)
    - se nombran igual que la clase
    */
    public Reloj() {
        this(0, 0, 0);
    }

    // Constructor por copia
    public Reloj(Reloj otro) {
        this(otro.horas, otro.getMinutos(), otro.getSegundos());
    }

    // Metodos de lectura / getters / accesors
    public int getHoras() {
        return horas;
    }

    // Metodos de escritura / setters / mutators
    public void setHoras(int horas) {
        if (horas >= MIN_HORAS && horas <= MAX_HORAS) {
            this.horas = horas;
        } else {
            System.out.printf("La hora esta en un rango de %d-%d%n",
                    MIN_HORAS, MAX_HORAS);
        }
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        if (segundos >= MIN_SEGUNDOS && segundos <= MAX_SEGUNDOS) {
            this.segundos = segundos;
        }
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        if (minutos >= MIN_MINUTOS && minutos <= MAX_MINUTOS) {
            this.minutos = minutos;
        }
    }

    public int getSegundosTranscurridos() {
        return this.horas * 60 * 60 + this.minutos * 60 + this.segundos;
    }

    public static int compare(Reloj primero, Reloj segundo) {
        if (primero.equals(segundo)) {
            return 0;
        } else {
            int segundos1 = primero.getSegundosTranscurridos();
            int segundos2 = segundo.getSegundosTranscurridos();

            if (segundos1 < segundos2) {
                return -1;
            }

            return 1;
        }
    }

    // Comportamiento de un objeto como String
    @Override
    public String toString() {
        String sh, sm, ss;

        if (horas < 10) {
            sh = "0" + horas;
        } else {
            sh = String.valueOf(horas);
        }

        if (minutos < 10) {
            sm = "0" + minutos;
        } else {
            sm = "" + minutos;
        }

        if (formato == EFormato.CORTO) {
            return sh + ":" + sm;
        } else {
            if (segundos < 10) {
                ss = "0" + segundos;
            } else {
                ss = "" + segundos;
            }

            return sh + ":" + sm + ":" + ss;
        }
    }

    public void setFormato(EFormato formato) {
        this.formato = formato;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reloj reloj = (Reloj) o;
        return segundos == reloj.segundos && minutos == reloj.minutos
                && horas == reloj.horas;
    }

    @Override
    public int hashCode() {
        return Objects.hash(segundos, minutos, horas);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}

// 05:30:40 -> numero
// 10:01:01 -> numero

// f.compare(g) <- no-static
// compare(f, g) <- static

// f.aumentar(5, HORAS)
// g.disminuir(3, MINUTOS)

// f = 05:59:34
// f.aumentar(10, MINUTOS)