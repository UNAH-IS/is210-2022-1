package hn.edu.unah.part2.reloj;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        Reloj r = new Reloj();
        Reloj o = new Reloj(7, 38, 0);

        // Copia superficial
        Reloj t = r;       // Aqui r = 00:00:00

        System.out.println("[t] justo despues de la copia");
        System.out.println(t);

        r.setSegundos(45);
        r.setMinutos(46);

        System.out.println(r);      // 00:45:46
        System.out.println(o);
        System.out.println("[t] despues de modificar [r]");
        System.out.println(t);      // ???

        t.setHoras(7);
        System.out.println("[t] fue modificado");
        System.out.println(r);
        System.out.println(t);

        Reloj r1 = new Reloj(o);
        r1.setHoras(45);
        o.setMinutos(59);
        System.out.println("r1 -> " + r1);
        System.out.println("o -> " + o);

        Reloj r2 = (Reloj) r1.clone();
        r2.setHoras(0);
        System.out.println("r1 -> " + r1);
        System.out.println("r2 -> " + r2);
    }
}
