package hn.edu.unah.part2.reloj;

public class MainGetSet {
    public static void main(String[] args) {
        Reloj r = new Reloj(32, 0, 0);
        System.out.println(r);

        r.setHoras(10);
        r.setHoras(100);
        r.setMinutos(40);
        r.setSegundos(23);
        r.setFormato(EFormato.LARGO);
        System.out.println(r);
    }
}
