package hn.edu.unah.part2.encapsulamiento.paq1;

public class Demo {
    public int AtributoPublico;
    protected int AtributoProtegido;
    private int AtributoPrivado;
    int AtributoDefault;

    // Mismo objeto
    public static void main(String[] args) {
        Demo d = new Demo();

        d.AtributoPrivado = 10;
        d.AtributoDefault = 100;
        d.AtributoProtegido = 10;
        d.AtributoPublico = 10;
    }
}
