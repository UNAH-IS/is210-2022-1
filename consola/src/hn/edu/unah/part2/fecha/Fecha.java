/*

    Clase:
        - conjunto componentes "especificos".
        - Plantilla que define como seran creados los "objetos".
        - Estructura: Atributos (variables), Metodos (funciones)

        [modificador] class [Identificador]

        ABSTRACCION - ENCAPSULAMIENTO

        String -> clase
        String s = "hola";
        s <- objeto

        String t = "como estan";

        s y t tienen la misma estructura interna. Lo que cambia es que s y t
        tienen "estados" diferentes

        static - indica que atributo/metodo pertenece a la clase
        y no al objeto

        1. Atributos
        2. Metodos
            2.1 Construir objetos -> constructores
            2.2 Son llamados cuando se destruye objetos -> destructores (*)
            2.3 Leer atributos -> Metodos de Acceso (Accesors/Getters)
            2.4 Modificar atributos -> Metodos modificadores (Mutators/Setters)
            2.5 Recorrer elementos de una coleccion de objetos -> Metodos iteradores (Iterators)

+----------------------------+
| Funciones                  |
|                            |
|     +-------------+        |
|     | Atributos   |        |
|     +-------------+        |
|                            |
+----------------------------+
 */

package hn.edu.unah.part2.fecha;

import java.util.Objects;

// Debe manejar los valores del dia, mes y año
public class Fecha implements Cloneable {
    /**************************************************************************
                                    ATRIBUTOS
     **************************************************************************/

    // Constantes
    final int MIN_DIAS = 1;
    final int MAX_DIAS = 31;
    final int MIN_AÑO = 1;
    final int MAX_AÑO = 9999;

    // Enum
    public enum EMeses {
        ENERO(1),
        FEBRERO(2),
        MARZO(3),
        ABRIL(4),
        MAYO(5),
        JUNIO(6),
        JULIO(7),
        AGOSTO(8),
        SEPTIEMBRE(9),
        OCTUBRE(10),
        NOVIEMBRE(11),
        DICIEMBRE(12);

        private int numeroMes;
        private int cantidadDias;

        EMeses(int numeroMes) {
            this.numeroMes = numeroMes;
        }

        public int getNumeroMes() {
            return this.numeroMes;
        }
    }

    public enum EPeriodo {
        DIA, MES, AÑO
    }

    // Son numeros que van del 1 al 31
    int dia;
    // Son numeros que van del 1 al 12
    private int mes;
    // Son numeros que van del 1 al 9999
    private int año;

    /**************************************************************************
                                    METODOS
     **************************************************************************/

    // Metodo constructor con tres parametros
    public Fecha(int d, int m, int a) {
        this.setDia(d);
        this.setMes(m);
        this.setAño(a);
    }

    // Metodo constructor sin parametros
    public Fecha() {
        this(1, 1, 2022);
    }


    // Metodo constructor con un parametro (por copia)
    public Fecha(Fecha otro) {
        this(otro.getDia(), otro.getMes(), otro.getAño());
    }

    @Override
    public String toString() {
        return "Fecha{" +
                "dia=" + dia +
                ", mes=" + getMes() +
                ", año=" + getAño() +
                '}';
    }

    /*
    Metodos de lectura (getters)
    Por lo general:
    - public (mayoria)
    - retornar un tipo (distinto a void)
    - la funcion no tiene parametro
    - inician con 'get'
     */
    public int getDia() {
        return dia;
    }

    /*
    Metodos de escritura (setters)
    Por lo general:
    - public
    - el tipo de retorno es void
    - la funcion si tiene parametros, los cuales el atributo
    - inician con 'set'
     */
    public void setDia(int dia) {
        // this.dia -> atributo ya que this representa al futuro objeto
        // dia -> parametro de la funcion

        if (dia >= MIN_DIAS && dia <= MAX_DIAS) {
            this.dia = dia;
        } else {
            this.dia = MIN_DIAS;
        }
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        if (mes >= EMeses.ENERO.getNumeroMes() && mes <= EMeses.DICIEMBRE.getNumeroMes()) {
            this.mes = mes;
        } else {
            this.mes = 1;
        }
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        if (año >= MIN_AÑO && año <= MAX_AÑO) {
            this.año = año;
        }
    }

    public double getAñoFraccion() {
        double resultado = this.año;
        resultado += this.mes / 12.0;
        resultado += this.dia / 365.0;

        return resultado;
    }

    public static int compare(Fecha o1, Fecha o2) {
        if (o1.equals(o2)) {
            // o1 == o2 => 0
            return 0;
        } else {
            // o1 < o2 => -1
            if (o1.getAñoFraccion() < o2.getAñoFraccion()) {
                return -1;
            }
            // o1 > o2 => 1
            return 1;
        }
    }

    public int compare(Fecha otraFecha) {
        if (this.equals(otraFecha)) {
            // o1 == o2 => 0
            return 0;
        } else {
            // o1 < o2 => -1
            if (this.getAñoFraccion() < otraFecha.getAñoFraccion()) {
                return -1;
            }
            // o1 > o2 => 1
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fecha fecha = (Fecha) o;
        return dia == fecha.dia && mes == fecha.mes && año == fecha.año;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dia, mes, año);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public boolean esAñoBisiesto() {
        int año = this.getAño();

        // 1. div entre 4
        // 2. div entre 400 si es div entre 100
        boolean isDiv400 = (año % 400 == 0);
        boolean isDiv100 = (año % 100 == 0);
        boolean isDiv4 = (año % 4 == 0);

        if (isDiv400 || (isDiv4 && !isDiv100)) {
            return true;
        }

        return false;
    }

    private void incrementarDiario(int valor) {
        // TODO: Calcular para valor si es positivo
        // TODO: Verificar valor si es negativo
        // TODO: Establecer si el año es bisiesto (caso 28-29 febrero)
        // TODO: Si el mes termina en 31 y al incrementar cae en un mes de menos de 31 dias.
    }

    private void incrementarMensual(int valor) {
        // TODO: Verificar valor si es negativo
        // TODO: Establecer si el año es bisiesto (caso 28-29 febrero)
        // TODO: Si el mes termina en 31 y al incrementar cae en un mes de menos de 31 dias.
        int meses = this.getMes() + valor;
        int incrementoAnual = meses / 12;
        int incrementoMensual = meses % 12;
        this.setAño(this.getAño() + incrementoAnual);
        this.setMes(incrementoMensual);
    }

    private void incrementarAnual(int valor) {
        // TODO: Verificar valor si es negativo
        // TODO: Establecer si el año es bisiesto (caso 28-29 febrero)
        // TODO: Si el mes termina en 31 y al incrementar cae en un mes de menos de 31 dias.
        this.setAño(this.getAño() + valor);
    }

    public void incrementar(int delta, EPeriodo periodo) {
        switch (periodo) {
            case DIA -> incrementarDiario(delta);
            case MES -> incrementarMensual(delta);
            case AÑO -> incrementarAnual(delta);
        }
    }
}
