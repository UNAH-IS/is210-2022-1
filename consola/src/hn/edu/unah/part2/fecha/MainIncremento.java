package hn.edu.unah.part2.fecha;

public class MainIncremento {
    public static void main(String[] args) {
        Fecha f = new Fecha(30, 1, 2000);
        f.incrementar(1, Fecha.EPeriodo.MES);
        f.incrementar(10, Fecha.EPeriodo.AÑO);
        System.out.println(f);

        // test para probar esAñoBisiesto
        f.setAño(1900);
        System.out.println("false -> " + f.esAñoBisiesto());
        f.setAño(2000);
        System.out.println("true -> " + f.esAñoBisiesto());
        f.setAño(2004);
        System.out.println("true -> " + f.esAñoBisiesto());
        f.setAño(2005);
        System.out.println("false -> " + f.esAñoBisiesto());

        int x = 3;

        System.out.println(x / 12); // años
        System.out.println(x % 12); //  meses restantes
    }
}
