package hn.edu.unah.part2.fecha;

public class MainCopia {
    public static void main(String[] args) throws CloneNotSupportedException {
        int x = 0;
        Fecha fecha = new Fecha(10, 3, 2022);

        // Referencia a la direccion de memoria de [fecha]. Se denomina copiado
        // superficial
        Fecha fechaCopia1 = fecha;

        // Para evitar el copiado superficial
        // Clonar el objeto
        Fecha fechaClonada = (Fecha) fecha.clone();
        fechaClonada.setMes(2);
        fecha.setAño(2050);

        System.out.println(fechaClonada);
        System.out.println(fecha);

        // Constructor por copia
        Fecha fechaCopia = new Fecha(fecha);
        fechaCopia.setMes(5);
        fecha.setAño(2040);

        System.out.println(fechaCopia);
        System.out.println(fecha);

        fecha.setDia(24);
        System.out.println(fecha.getDia());
    }
}
